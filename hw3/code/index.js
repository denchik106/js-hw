/*ДЗ 3:
 Создайте массив styles с элементами «Джаз» и «Блюз».
Добавьте «Рок-н-ролл» в конец.
Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
Удалите первый элемент массива и покажите его.
Вставьте «Рэп» и «Регги» в начало массива.
*/

const styles = [`Джаз`, `Блюз`];
console.log('Начальний массив');
console.log(styles);
//Добавляем элемент в конец массива
styles.push(`Рок-н-ролл`);

console.log(`Добавляем элемент в конец массива`);
console.log(styles);

//Заменяю элемент в середине на «Классика».
styles.splice(styles.length / 2, 1); //удаляю один элемент всередине массива
console.log(` Заменяю элемент в середине на «Классика» Удаляю один элемент всередине массива`);
console.log(styles);

styles.splice(styles.length / 2, 0, `Классика`);// добавляю на место удаленного элемента новый объект
console.log(`Заменяю элемент в середине на «Классика» Добавляю на место удаленного элемента новый объект`);
console.log(styles);

//удаляю первый элемент массива
let el = styles.shift(0)
console.log(`Удаляю первый элемент массива`)
console.log(styles);

//Вывожу на экран первый элемент массива
document.write(el);

//Добавляю элементы в начало массива
styles.unshift(`Рэп`, `Регги`);
console.log(`Добавляю элементы в начало массива`);
console.log(styles)