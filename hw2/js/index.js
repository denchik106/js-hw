/*
Задача 1 
Використовуючи пробіли &nbsp; та зірочки * намалюйте використовуючи цикли
Трикутник
Ромб
Порожній прямокутник */

//Трикутник

let row = 10;

for (let i = 0; i < row; i++) {
    for (let j = 0 + i; j < row; j++) {
        document.write("&nbsp");
    }
    for (let z = 10 - i; z < row; z++) {
        document.write(`*`)
    }
    document.write(`<br>`)
};


// Трикутник(2 вариант)

// let d = 9;
// let e = 1;

// for (let i = 0; i < 10; i++) {
//     for (let b = 0; b < d; b++) {
//         document.write(`&nbsp\n`)
//     };
//     for (let c = 0; c < e; c++) {
//         document.write(`*`)
//     };

//     d -= 1;
//     e += 2;

//     document.write(`<br/>`)
// };



//Ромб

row = 20;

for (let i = 0; i < row; i++) {
    if (i < 10) {
        for (let j = 0 + i; j < 10; j++) {
            document.write("&nbsp");
        }
        for (let z = 10 - i; z < 10; z++) {
            document.write(`*`)
        }
        document.write(`<br>`)
    } else if (i >= 10) {

        for (let j = 20 - i; j < 10; j++) {
            document.write("&nbsp");
        }
        for (let z = 0 + i; z < 20; z++) {
            document.write(`*`)
        }
        document.write(`<br>`)
    }

};

// Ромб(2 вариант)

// row  = 8;
// let space = 9;
// let star = 1;

// for (let i = 0; i < row; i++) {
//     for (let b = 0; b < space; b++) {
//         document.write(`&nbsp\n`)
//     };
//     for (let c = 0; c < star; c++) {
//         document.write(`*`)
//     };

//     space -= 1;
//     star += 2;

//     document.write(`<br/>`)
// };

// row = 9;

// for (let i = 0; i < row; i++) {
//     for (let b = 0; b < space; b++) {
//         document.write(`&nbsp\n`)
//     };
//     for (let c = 0; c < star; c++) {
//         document.write(`*`)
//     };

//     space += 1;
//     star -= 2;

//     document.write(`<br/>`)
// };




//Порожній прямокутник

row = 10;
length = 30;

for (let i = 0; i < row; i++) {
    if (i === 0 || i === 9) {
        for (let j = 0; j < length; j++) {
            document.write(`*`)
        }
    } else {
        for (let j = 0; j < 1; j++) {
            document.write(`*`)
        }
        for (let c = 0; c < (length - 2); c++) {
            document.write(`&nbsp\n`)
        }
        for (let j = 0; j < 1; j++) {
            document.write(`*`)
        }
    }
    document.write(`<br>`);
}


/*
Задача 2

Реалізувати програму на Javascript, яка буде знаходити всі числа кратні 5 (діляться на 5 без залишку) у заданому діапазоні.

- Вивести за допомогою модального вікна браузера число, яке введе користувач.
- Вивести в консолі усі числа кратні 5, від 0 до введеного користувачем числа. Якщо таких чисел немає - вивести в консоль фразу Sorry, 
no numbers
- Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
- Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується, повторювати виведення вікна на екран, 
доки не буде введено ціле число.
 */


let num = prompt(`Введите число`);

alert(`Вы ввели число ${num}`);

if (num !== null) {
    while (num % 1 !== 0) {
        alert(`Введите целое число`);
        num = parseFloat(prompt(`Введите целое число`));
    }
    if (num > 0) {
        for (i = 0; i <= num; i++) {
            if (i % 5 === 0) {

                console.log(`числа кратні 5:  ${i}`);
            }
        }
    } else {
        console.error(`Sorry, no numbers`)
    }
} else (
    alert(`Отмена операции`)

)

/*
- Вважати два числа, m та n. Вивести в консоль усі прості числа 
(http://ua.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_% D1%87%D0%B8%D1%81%D0%BB%D0%BE)
 в діапазоні від m до n (менше з введених чисел буде m, більше буде `n`).
  Якщо хоча б одне з чисел не дотримується умови валідації, вказаної вище, вивести повідомлення про помилку і запитати обидва числа заново.
*/

let m = prompt(`Введите  меньшее число m`);
let n = prompt(`Введите большее число n`);

if (m >= n || m === null || n === null) {
    while (m >= n) {
        alert('Ошибка');
        m = prompt(`Введите число!`);
        n = prompt(`Это число должно быть больше!`);
    }
} else {

    nextStep:
    for (let i = m; i <= n; i++) {

        for (let j = 2; j < i; j++) {
            if (i % j == 0)
                continue nextStep;
        }
        console.log(`Прості числа: ${i}`);

    }

}























