//1 задание========================================================================================================================

//Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ

const div150 = document.getElementById("div150");
const div = document.createElement("div")
div.style.boxSizing = "border-box"
div.id = "divst";
div150.append(div);

const getdiv = document.createElement("div");
getdiv.style.boxSizing = "border-box"
getdiv.style.margin = "150px";
getdiv.style.fontSize = "24px";
getdiv.innerHTML = "Hello!";
div.appendChild(getdiv);

let margin = window.getComputedStyle(getdiv).getPropertyValue("margin");

console.log(margin);

//2 задание=========================================================================================================================

/*Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції*/



const minEl = document.querySelector(".min");
const secEl = document.querySelector(".sec");
const millEl = document.querySelector(".mill");

const btnStart = document.querySelector("#start");
const btnStop = document.querySelector("#stop");
const btnReset = document.querySelector("#reset");

const colorDisplay = document.querySelector(".stopwatch-display");

let minute = 00,
    second = 00,
    millisecond = 00,
    getinfo = 0;

btnStart.onclick = () => {
    colorDisplay.classList.add("green");
    colorDisplay.classList.remove("red");
    colorDisplay.classList.remove("silver");
    clearInterval(getinfo);
    getinfo = setInterval(startTimer, 10)

};

btnStop.onclick = () => {
    colorDisplay.classList.add("red");
    colorDisplay.classList.remove("silver");
    colorDisplay.classList.remove("green");
    clearInterval(getinfo);
};

btnReset.onclick = () => {
    colorDisplay.classList.add("silver");
    colorDisplay.classList.remove("red");
    colorDisplay.classList.remove("green");
    clearInterval(getinfo);
    minute = 00;
    second = 00;
    millisecond = 00;
    millEl.innerText = "00";
    secEl.innerText = "00";
    minEl.innerText = "00";
};



let startTimer = () => {
    millisecond++
    if (millisecond < 9) {
        millEl.innerText = "0" + millisecond;
    }

    if (millisecond > 9) {
        millEl.innerText = millisecond;
    }

    if (millisecond == 100) {
        second++;
        secEl.innerText = "0" + second;
        millisecond = 0;
        millEl.innerText = millisecond;
    }

    if (second < 9) {
        secEl.innerText = "0" + second;
    }

    if (second > 9) {
        secEl.innerText = second;
    }

    if (second == 60) {
        minute++;
        minEl.innerText = "0" + minute;
        second = 0;
        secEl.innerText = second;
    }

    if (minute < 9) {
        minEl.innerText = "0" + minute;
    }

    if (minute > 9) {
        minEl.innerText = minute;
    }

    if (minute == 60) {
        minute = 0;
    }

};

//3 задание======================================================================================================================

/*  Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера,
 якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку 
 https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input.*/


let watch = document.querySelector(".watch");
let crdiv = document.createElement("div");
crdiv.className = "phone";
watch.after(crdiv);

let tel = document.createElement("input");
tel.style.padding = "5px";
tel.style.display = "block";
tel.style.margin = "10px auto";
tel.type = "text";
crdiv.appendChild(tel);

let btn = document.createElement("input");
btn.style.display = "block";
btn.style.padding = "5px";
btn.style.margin = "0 auto";
btn.value = "Зберегти";
btn.type = "button";
btn.innerHTML = "Сохранить";
crdiv.appendChild(btn);

let errot = document.createElement("div");
errot.style.textAlign = "center";
errot.style.margin = "10px";
errot.innerHTML = "Введите корректное значение (000-000-00-00)";

const pttern = /\d{3}-\d{3}-\d{3}-\d{2}-\d{2}/;

let url = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";



btn.onclick = () => {
    if (pttern.test(tel.value)) {
        tel.style.backgroundColor = "green";
        //  document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
        window.open(url, '_blank');
        errot.remove();
    } else
        tel.before(errot);
}


//4 задание==========================================================================================================================
/* Слайдер */


let slides = document.querySelectorAll('#slides .slide');
let currentSlide = 0;
let slideInterval = setInterval(nextSlide, 3000);

function nextSlide() {
    slides[currentSlide].className = 'slide';
    currentSlide = (currentSlide + 1) % slides.length;
    slides[currentSlide].className = 'slide showing';
}









