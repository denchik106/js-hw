// - При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
// - Поведінка поля має бути такою:
// - При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
// - Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
// . 
// Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
// - При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
// - Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
// під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.



document.addEventListener(`DOMContentLoaded`, () => {
    let div = document.querySelector(`.task4`),
        divSpan = document.querySelector(`.span`),
        input = document.createElement(`input`),
        span = document.createElement(`span`),
        button = document.createElement(`button`),
        p = document.createElement(`p`);

    input.placeholder = `Price`;
    input.type = `number`;
    button.innerText = `X`;

    div.append(input);

    input.addEventListener(`focus`, (event) => {
        input.classList.add('ok');
        input.classList.remove(`error`);
    })

    input.addEventListener(`blur`, () => {
        if (isNaN(input.value) || input.value < 0) {
            input.classList.remove(`ok`);
            input.classList.add('error');
            p.innerText = `Please enter correct price`;
            input.value = ``;
            div.append(p);
            button.remove();
            span.remove();
        } else if (input.value == ``) {
            input.classList.remove(`ok`);
            p.innerText = ``;
        } else {
            input.classList.remove(`error`);
            input.classList.add('ok');
            span.innerText = input.value;
            divSpan.append(span);
            divSpan.append(button);
            p.remove();
        }
        button.addEventListener(`click`, () => {
            span.remove();
            button.remove();
            input.value = ``;
            input.classList.remove(`error`);
            input.classList.remove(`ok`);
        })
    })
})