// Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів.
// Скільки символів має бути в інпуті, зазначається в атрибуті data - length.Якщо вбито правильну кількість,
// то межа інпуту стає зеленою, якщо неправильна – червоною.

let [...inputLenght] = document.querySelectorAll(`.length`);

inputLenght.forEach((el) => {
    if (el.type == `text`) {
        el.setAttribute(`data-lenght`, 20)
        el.placeholder = `Введите ${el.dataset.lenght} символов!`
    } else {
        el.setAttribute(`data-lenght`, 13)
        el.placeholder = `Введите ${el.dataset.lenght} символов!`
    }

    el.addEventListener(`input`, () => {
        if (parseInt(el.dataset.lenght) == el.value.length) {
            el.classList.add('ok');
            el.classList.remove(`error`);
        } else {
            el.classList.add('error');
            el.classList.remove(`ok`);
        }
    })

})

