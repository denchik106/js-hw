const arrLogo = [
    {
        "KyivStar": [`039`, `067`, `068`, `096`, `097`, `098`],
        "link": `./image/kisspng-kyivstar-ukraine-mobile-service-provider-company-l-5ae1f72f32a5d9.1825174915247583192075.jpg`
    },
    {
        "Vodafone(MTS)": [`050`, `066`, `095`, `099`],
        "link": `./image/kisspng-logo-vodafone-brand-vector-graphics-trademark-vodafone-logo-png-white-www-pixshark-com-image-5bff615f687d54.186717641543463263428.jpg`
    },
    {
        "Lifecell(Life)": [`063`, `073`, `093`],
        "link": `./image/1307202012_lifecell-500x500.jpg`
    },
    {
        "Utel": [`091`],
        "link": `./image/Без названия.png`
    },
    {
        "PEOPLEnet": [`092`],
        "link": `./image/kisspng-peoplenet-logo-ukraine-mobile-web-internet-5bfaccfce71ef1.0111588715431631329467.jpg`
    },
    {
        "Intertelecom": [`089`, `094`],
        "link": `./image/unnamed.jpg`
    }
]

export default arrLogo;