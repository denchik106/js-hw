// Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні.
// Ім'я, Прізвище (Українською)
// Список з містами України 
// Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
// Пошта 
// Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅
// Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані 

import city from "./uacity.js";
import arrLogo from "./arrLogo.js";

const inputs = document.querySelectorAll(`.task3`),
    btn = document.querySelector(`.btn`),
    listCity = document.querySelector(`#data-city`),
    img = document.createElement(`img`);

city.forEach((el) => {
    let option = document.createElement("option");
    option.value = el.city;
    listCity.append(option)
}
)

function validate(p, e) {
    return p.test(e)
}

function inputLink() {
    let el = this;
    if (el.id == `name`) {
        if (validate(/^[А-яЇїЄєІі-]+$/, el.value)) {
            el.nextSibling.replaceWith(`✅`)
        } else {
            el.nextSibling.replaceWith(`❌`)
        }

    } else if (el.id == `lname`) {
        if (validate(/^[А-яЇїЄєІі-]+$/, el.value)) {
            el.nextSibling.replaceWith(`✅`)
        } else {
            el.nextSibling.replaceWith(`❌`)
        }

    } else if (el.id == `city`) {
        let [...cityArr] = listCity.children;
        for (let i = 0; i < cityArr.length; i++) {
            if (cityArr[i].value == el.value) {
                el.nextSibling.replaceWith(`✅`)
                break;
            } else {
                el.nextSibling.replaceWith(`❌`)
            }
        }

    } else if (el.id == `phone`) {
        if (validate(/^\+380\d{2} \d{3} \d{2} \d{2}$/, el.value)) {
            const [...item] = el.value
            let rez = item.splice(3, 3).join(``);
            let flag = false;

            arrLogo.forEach((elem) => {
                let a = [];
                let link;
                for (let key in elem) {
                    if (Array.isArray(elem[key])) {
                        a = elem[key];
                    } else {
                        link = elem[key]
                    }
                };
                for (let i = 0; i < a.length; i++) {
                    if (rez == a[i]) {
                        img.src = link;
                        el.nextSibling.replaceWith(`✅`);
                        el.nextSibling.after(img)
                        flag = true;
                    }
                }
            })
            if (flag != true) {
                el.nextSibling.replaceWith(`❌`)
                img.remove();
            }

        } else {
            el.nextSibling.replaceWith(`❌`);
            img.remove();
        }
    } else if (el.id == `mail`) {
        if (validate(/^[A-z0-9_!@#$%^&*]+@[A-z]{2,5}.[A-z]{2,4}.[A-z]{2,4}$/, el.value)) {
            el.nextSibling.replaceWith(`✅`)
        } else {
            el.nextSibling.replaceWith(`❌`)
        }
    }
}

inputs.forEach((el) => {
    el.addEventListener(`blur`, inputLink)
})

btn.addEventListener(`submit`, (e) => {
    inputs.forEach(function (el) {
        inputLink.apply(el);
    })
    e.preventDefault;
})


