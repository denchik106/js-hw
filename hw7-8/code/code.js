/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". 
А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
 */

class Driver {
    constructor(fullName, drivingExperience) {
        this.fullName = fullName;
        this.drivingExperience = drivingExperience;
    }
    show(){
        return (`Им'я користувача: ${this.fullName}, </br> Стаж водіння: ${this.drivingExperience}`) 
    }
};

const driver = new Driver(`Ivan`, `15 years`);
console.log(driver.show());

class Engine {
    constructor(power, manufacturer) {
        this.power = power;
        this.manufacturer = manufacturer;
    }
    show(){
        return (`Потужність: ${this.power}, </br> Виробник: ${this.manufacturer}`)  
    }
};

const engine = new Engine(`100_hp`, `Renault`);
console.log(engine.show())

class Car {
    constructor(carBrand, carClass, weight, driver, engine) {
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.weight = weight;
        this.Driver = driver;
        this.Engine = engine;
    };

    start() {
        return "Поїхали"
    };
    stop() {
        return "Зупиняємося"
    };
    turnRight() {
        return "Поворот праворуч"
    };
    turnLeft() {
        return "Поворот ліворуч"
    };
    toString() {
        return (`Марка авто: ${this.carBrand}, </br> Клас авто: ${this.carClass}, </br> Вага:  ${this.weight}, 
        </br> Водій </br> ${this.Driver.show()}, </br> Мотор типу Engine: </br> ${this.Engine.show()}, </br> 
    `)
    }
};

const car = new Car(`Clio`, `hatchback`, `1100kg`, driver, engine);

document.querySelector(`#car`).innerHTML =  car.toString();


class Lorry extends Car {
    constructor(carBrand, carClass, weight, driverTypeDriver, motorTypeEngine, carryingBody) {
        super(carBrand, carClass, weight, driverTypeDriver, motorTypeEngine)
        this.carryingBody = carryingBody;
    }
};

class SportCar extends Car {
    constructor(carBrand, carClass, weight, driverTypeDriver, motorTypeEngine, maxSpeed) {
        super(carBrand, carClass, weight, driverTypeDriver, motorTypeEngine)
        this.maxSpeed = maxSpeed;
    }
};











































// class Hero {
//     constructor(name, level) {
//         this.name = name;
//         this.level = level;
//     }

//     // Adding a method to the constructor
//     greet() {
//         return `${this.name} says hello.`;
//     }
// };

// const hero1 = new Hero('Varg', 1);

// console.log(hero1.greet(hero1));

// class Mage extends Hero {
//     constructor(name, level, spell) {
//         // Chain constructor with super
//         super(name, level);

//         // Add a new property
//         this.spell = spell;
//     }
// }

// const hero2 = new Mage('Lejon', 2, 'Magic Missile');

// console.log(hero2.greet());

