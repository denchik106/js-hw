// Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени и
// фамилии

class User {
    constructor(name, lastName) {
        this.name = name;
        this.lastName = lastName;
    }
    showName = () => {
        console.log(`${this.name}  ${this.lastName}`)
    }
}


let users = new User(`Hello`, `World`);
users.showName()

// Создай список состоящий из 4 листов. Используя джс обратись к 2 li и с использованием навигации по DOM дай 1 элементу
// синий фон, а 3 красный

let ul = document.getElementById(`ul`)
let li = ul.children[1];
li.previousElementSibling.style.color = `blue`;
li.nextElementSibling.style.color = `red`;

// Создай див высотой 400 пикселей и добавь на него событие наведения мышки. При наведении мышки выведите текстом
// координаты, где находится курсор мышки

let cube = document.querySelector(`.cube`);
cube.style.border = `1px solid black`;
cube.addEventListener(`mousemove`, (event) => {
    cube.textContent = `Координата Х: ${event.offsetX}
                     Координата Y: ${event.offsetY}`
})


// Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата;

let [...btn] = document.querySelectorAll(`.btn`);
btn.forEach((e) => { e.addEventListener(`click`, (event) => { console.log(event.target.innerText) }) })



// Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице

let banner = document.querySelector(`.banner`);
console.log(banner);

banner.addEventListener(`mouseover`, function (event) {
    event.target.style.position = `absolute`;
    event.target.style.top = Math.floor(Math.random() * 100) + `vh`;
    event.target.style.left = Math.floor(Math.random() * 100) + `vw`;
})


// Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body


let colorBody = document.getElementById(`color`);
colorBody.addEventListener(`blur`, (e) => {
    document.body.style.backgroundColor = e.target.value;
})

// Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль

let log = document.getElementById(`login`);
login.addEventListener(`input`, (e) => {
    console.log(e.target.value);
})

// Создайте поле для ввода данных поле введения данных выведите текст под полем

let rez = document.getElementById(`rez`)

login.addEventListener(`change`, (e) => {
    rez.innerText = e.target.value;
})

