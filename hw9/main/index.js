/* Нарисовать на странице круг используя параметры, которые введет пользователь.

    При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг".
    Данная кнопка должна являться единственным контентом в теле HTML документа,
    весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
    При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга.
    При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета.
    При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться,
    то есть все остальные круги сдвигаются влево.
*/

const btn = document.getElementById(`circle`);

btn.onclick = function () {
  btn.remove();
  
  const inp = document.createElement(`input`);
  inp.id = "cr";
  inp.style.textAlign = `center`
  inp.style.margin = "20px auto"
  inp.style.fontSize = "24px"
  inp.value;
  inp.placeholder = `Введите диаметр круга`;
  document.body.querySelector(".dv").appendChild(inp);
  const submit = document.createElement(`input`);
  submit.type = "button";
  submit.id = "sb"
  submit.value = "Нарисовать";
  submit.style.display = "block";
  submit.style.textAlign = "center";
  submit.style.fontSize = "24px"
  submit.style.margin = "0 auto 10px"
  document.body.querySelector(".dv").appendChild(submit);

  submit.onclick = function () {
    for (let i = 0; i < 100; i++) {
      const circles = document.createElement(`div`);
      circles.style.display = "inline-block";
      circles.style.width = `${inp.value}px`;
      circles.style.height = `${inp.value}px`;
      circles.style.borderRadius = "50%"
      circles.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
      circles.style.margin = "0 auto";
      circles.className = "circle";
      document.body.append(circles);
    };

    let [...del] = document.querySelectorAll(".circle");
    del.forEach(el => {
      el.onclick = function () {
        el.remove();
      }
    }
    );
  }
};

//Classwork==============================================================================================================


//1 задание

/*Реализовать функцию для создания объекта "пользователь".
Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
 Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
 соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
 Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
 Вывести в консоль результат выполнения функции.*/


// function CreateNewUser() {
//   this.name = prompt(`name`);
//   this.lastName = prompt(`sname`);
//   this.getLogin = function () {
//     let rez = this.name.charAt(0).toLowerCase() + this.lastName.toLowerCase();
//     return rez;
//   }

// }

// let newUser = new CreateNewUser();
// console.log(newUser.getLogin());


//2 задание====================================================================================================================

/*Создайте 2 инпута и одну кнопку. Сделайте так чтобы инпуты обменивались содержимым. */

// function inp() {
//   let input1 = document.createElement("input");
//   input1.type = "text";
//   input1.id = "input1"
//   input1.value;
//   document.body.querySelector(".in").appendChild(input1);

//   let input2 = document.createElement("input");
//   input2.id = "input2"
//   input1.type = "text";
//   input2.value;
//   document.body.querySelector(".in").appendChild(input2);

//   let but = document.createElement(`input`);
//   but.type = "button";
//   but.value = "Обмен информацией";
//   document.body.querySelector(".in").appendChild(but);

//   but.onclick = function () {
//   [input1.value, input2.value]=[input2.value, input1.value]

//   };
// };

// inp();





//3 задание======================================================================================================================

//Создайте 5 дивов на странице затем используя getElementsByTagName и forEach поменяйте дивам цвет фона.


// const create = () => {
//   let cr = document.createElement("div")
//   cr.style.width = "100px";
//   cr.style.height = "100px";
//   cr.id = "divs";
//   document.body.append(cr);
//   document.body.querySelector(".in").appendChild(cr);
// }

// for(let i = 0; i < 5; i++){
//   create();
// }

// // const [...colorDiv] = document.getElementsByTagName("div");

// const [...colorDiv] = document.querySelector("div").children;
// colorDiv.forEach(element => {
//   element.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`
// })


// 4 задание=====================================================================================================================

/* Создайте многострочное поле для ввода текста и кнопку. После нажатия кнопки пользователем приложение должно
сгенерировать тег div с текстом который был в многострочном поле. многострочное поле следует очистить после
переимещения информации
*/


// const area = document.createElement("textarea");
// area.id = "area";
// area.placeholder = "Введите значение"
// area.value;
// document.body.append(area);

// const btn9 = document.createElement("input");
// btn9.type = "button";
// btn9.style.display = "block";
// btn9.value = "генерировать тег div"
// document.body.append(btn9);

// btn9.onclick = function(){
//   const div = document.createElement("div");
//   div.innerHTML = area.value;
//   document.body.append(div);
//    area.value = "";
// }


//5 задание======================================================================================================================

/*Создайте картинку и кнопку с названием "Изменить картинку"
сделайте так чтобы при загрузке страницы была картинка
https://itproger.com/img/courses/1476977240.jpg
При нажатии на кнопку перый раз картинка заменилась на
https://itproger.com/img/courses/1476977488.jpg
при втором нажатии чтобы картинка заменилась на
https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png
*/

// const image = document.createElement("img");
// image.src = "https://itproger.com/img/courses/1476977240.jpg"
// image.style.width = "50vw";
// image.style.maxHeight = "70vh";
// document.body.append(image);


// const btn99 = document.createElement("input");
// btn99.type = "button";
// btn99.value = "Изменить картинку";
// btn99.id = "btn99";
// btn99.style.display = "block";
// document.body.append(btn99);


// btn99.onclick = function () {
//   image.src = "https://itproger.com/img/courses/1476977488.jpg";
//   btn99.onclick = function () {
//     image.src = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png";
//   }
// };



//6 задание=================================================================================================================================

/*Создайте на странице 10 параграфов и сделайте так чтобы при нажатии на параграф он исчезал*/

// function createp() {
//   let p = document.createElement("p");
//   p.className = "className";
//   p.innerHTML = "Рудь";
//   p.style.backgroundColor = 'red';
//   document.body.append(p);
// }

// for (let i = 0; i < 10; i++) {
//   createp();
// }


// const [...remov] = document.querySelectorAll(".className")
// remov.forEach(el => {
//  el.onclick = function () {
//   el.style.visibility = "hidden";
// };
// }
// );












