/*
Напишіть функцію isEmpty(obj), яка повертає true, якщо об'єкт не має властивостей, інакше false.
*/
let object = {
  // name: `ivan`
};
function isEmpty(obj) {
  for (let key in obj) {
    return true;
  }
  return false;
}

console.log(isEmpty(object));

/*
Разработайте функцию-конструктор, которая будет создавать объект Human (человек). Создайте массив объетов и реализуйте 
функцию, которая будет сортировать элементы массива по значению свойства Age по возрастанию или по убыванию.

Разработайте функцию-конструктор, которая будет создавать объект Human (человек), добавьте на свое усмотрение свойства и 
методы в этот объект. Подумайте, какие методы и свойства следует сделать уровня экземпляра, а какие уровня функции-конструктора.   
*/


function Human(name, sname, age, gender) {
  this.name = name;
  this.sname = sname;
  this.age = age;
  this.gender = gender;

  this.info = function () {
    return `Меня зовут: ${this.name}. <br> Мой возраст: ${this.age}. <br>`;
  }
};

const humans = [
  new Human(`Andrey`, `Andreev`, 34, `male`),
  new Human(`Vasya`, `Prokopenko`, 22, `male`),
  new Human(`Tanya`, `Slysar`, 37, `female`),
  new Human(`veronika`, `Korobok`, 65, `male`)
];

Human.prototype.showAll = function () {
  document.querySelector(`.koko`).innerHTML += `<p>Имя: ${this.name}, Фамилия: ${this.sname}, Возраст:  ${this.age}, Пол: ${this.gender},</p>`
};

function countHuman() {
  return humans.length
}

function sortage(arr) {
  arr.sort(function (a, b) {
    if (a.age < b.age) {
      return 1;
    }
    if (a.age > b.age) {
      return -1;
    }
    return 0;
  });
}

function hello() {
  humans.forEach((el) => {
    el.showAll();
  })
}

sortage(humans);
hello();
console.log(countHuman());

















